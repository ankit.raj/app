class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email])
    session[:current_user_id] = @user.id
    if @user && @user.authenticate(params[:session][:password])
      redirect_to "/articles/new"
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end


  def destroy
    session[:user_id] = nil
  	redirect_to_root_url
  end
end
